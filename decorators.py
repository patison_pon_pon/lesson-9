import requests


def first(msg):
    print(msg)


first('Hello')

# second = first('Hello')
# print(second)
second = first
second('Hello')


def inc(x):
    return x + 1


def dec(x):
    return x - 1


def operate(func, x):
    result = func(x)
    return result


print(operate(inc, 3))
print(operate(dec, 3))


def wrapper_function():
    def hello_world():
        print('Hello world')
    return hello_world


new = wrapper_function()
new()


# def decorator_function(func):
#     def wrapper():
#         print('Функция-обёртка!')
#         print('Оборачиваемая функция: {}'.format(func))
#         print('Выполняем обёрнутую функцию...')
#         func()
#         print('Выходим из обёртки')
#     return wrapper
#
#
# @decorator_function
# def hello_world():
#     print('Hello world!')
#
#
# hello_world()


def decorator_function(func):
    def wrapper(name):
        print('Функция-обёртка!')
        print('Оборачиваемая функция: {}'.format(func))
        print('Выполняем обёрнутую функцию...')
        func(name)
        print('Выходим из обёртки')
    return wrapper


@decorator_function
def hello_world(name):
    print(f'Hello {name}!')


hello_world('Коля')


def benchmark(func):
    from datetime import datetime

    def wrapper():
        start = datetime.now()
        func()
        end = datetime.now()
        print(f'[*] Время выполнения: {end-start} секунд.')
    return wrapper


@benchmark
def fetch_webpage():
    requests.get('https://google.com')


fetch_webpage()


def star(func):
    def inner(*args, **kwargs):
        print('*' * 30)
        func(*args, **kwargs)
        print('*' * 30)
    return inner


def percent(func):
    def inner(*args, **kwargs):
        print("%" * 30)
        func(*args, **kwargs)
        print("%" * 30)
    return inner


# @star
# @percent
def printer(msg):
    print(msg)


printer('Hello')


printer = star(percent(printer))
printer('Hello')
