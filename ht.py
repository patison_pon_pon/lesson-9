def memoize_func(function):
    from datetime import datetime
    memo = dict()

    def wrapper(a, b):
        start = datetime.now()
        if (a, b) not in memo:
            memo[(a, b)] = function(a, b)
        else:
            print('from memo')
        end = datetime.now()
        print(f"Аргументы: {(a, b)}")
        print(memo[(a, b)])
        print(f'[*] Время выполнения: {end-start} секунд.')
        print('-' * 100)

    return wrapper


@memoize_func
def numbers_sum(a, b):
    number_sum = 0
    for number in range(a, b + 1):
        number_sum += number
    return number_sum


numbers_sum(3, 1000005)
numbers_sum(3, 1000004)
numbers_sum(3, 1000002)
numbers_sum(3, 1000005)
numbers_sum(3, 1000003)
numbers_sum(3, 1000004)
numbers_sum(3, 1000005)
numbers_sum(3, 1000007)
numbers_sum(3, 1000008)
numbers_sum(3, 1000007)
