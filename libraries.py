from faker import Faker


faker = Faker('uk_UA')

print(faker.name())
print(faker.address())
print(faker.phone_number())
print(faker.text())
print(faker.job())


print(f'Name: {faker.name()}')
print(f'First name: {faker.first_name()}')
print(f'Last name: {faker.last_name()}')

print('--------------------------')

print(f'Male name: {faker.name_male()}')
print(f'Female name: {faker.name_female()}')

