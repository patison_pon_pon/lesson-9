# set - множества - неупорядоченная коллекция уникальных элементов
rainbow_colors = {'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'}
print(rainbow_colors)
print(type(rainbow_colors))

empty_set = {}
print(empty_set)
print(type(empty_set))

empty_set = set()
print(empty_set)
print(type(empty_set))

number_list = [1, 43, 56, 10, 15, 17]
text_tuple = ('hello', 'world')

set_from_list = set(number_list)
set_from_tuple = set(text_tuple)
print(set_from_list)
print(set_from_tuple)

set_from_list.add(88)
set_from_list.add(88)
set_from_list.add(-88)
set_from_list.add(-92)
set_from_list.add(-100500)
set_from_tuple.add('hi')
print(set_from_list)
print(set_from_tuple)


x = set_from_list.pop()
y = set_from_list.remove(-100500)
#y = set_from_list.remove(-100500)
z = set_from_list.discard(88)
z = set_from_list.discard(88)
print(set_from_list)
print(x)
print(y)
print(z)
print(set_from_list.clear())

my_frozenset = frozenset('hello')
print(my_frozenset)
print(type(my_frozenset))
